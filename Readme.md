### How to ask for correction or other issues?

![Men Working](https://gitlab.com/iush/software-architecture-docs/raw/master/images/menworking.png "Document under maintenance!")

You can post a new issue on gitlab, over this repo:
[https://gitlab.com/iush/software-architecture-docs](https://gitlab.com/iush/software-architecture-docs)

Check this documentation if you dont know how create a new issue:
[https://about.gitlab.com/features/issueboard/](https://about.gitlab.com/features/issueboard/)

_
====

This article assumes you are interested in using containers and microservices on the docker way. We try give to you the concepts and even a basic example.

_
====

Objectives:
- Desmostrate some of the actual state of art on develop and deploy tools.
- Show the important space of Git (on the flavours of Github, GitLab).
- How develop and deploy using diferents tool that include several cain of registrires like DockerHub or GitLab.
- How the Open Source make the real software near to the new developers, actual parading of software develop.
- Explain (again) how the actual ways of sofware develop can potenciar the software develop on the academy ambite.

----

# Software Architecture Main Document
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/Wikipedia_4M1_Architectural_View_Model.png "Analogous to the different types of blueprints")](https://en.wikipedia.org/wiki/Software_architecture)

Software architecture descriptions are commonly organized into views, which are analogous to the different types of blueprints made in building architecture.

_
====

## Software Architecture blueprints
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/4m1_Updated.png "Different types of blueprints")](https://www.slideshare.net/ChrisCarroll2/xpmanchester-2013-software-architecture-for-agile-developers-intro)

Update Software architecture descriptions, which are analogous to the different types of blueprints made in building architecture.

_
====

## Basic Software Layers on Docker
![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/4m1_Updated_integrated.png "Different types of blueprints")

Where the basic model of Docker and specific DockerHub apply.

_
====

## Using great projects to gain experience
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/wikka_logo.jpg "WikkaWiki is a flexible, standards-compliant and lightweight wiki engine written in PHP.")](http://wikkawiki.org)

There are a great Open Source projects waiting for some help. New programers can get expertice working with real code or even deploy.

Principal GitHub repository:

[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/github.png "WikkaWiki repo")](https://github.com/wikkawik/WikkaWiki)

- Make a push on github.

_
====

## Colaborating for create a Usefull Wikka
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/wikka_logo.jpg "WikkaWiki is a flexible, standards-compliant and lightweight wiki engine written in PHP.")](http://wikkawiki.org)

DockerBuild repository:

[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/github.png "WikkaWiki Docker repo")](https://github.com/oemunoz/wikkawiki)

- Initiative.


_
====

## Build WikkaWiki plugins
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/wikka_logo.jpg "WikkaWiki is a flexible, standards-compliant and lightweight wiki engine written in PHP.")](https://hub.docker.com/u/oems/)

DockerBuild repository:

- [Wikka-md-handler](https://github.com/oemunoz/Wikka-md-handler)
- [wikka-odf-action](https://github.com/pepitosoft/wikka-odf-action)
- [Wikka REST API](https://github.com/pepitosoft/wikka-api)
- [WikkaWiki Force Action plugin](https://github.com/pepitosoft/force)
- [WikkaWiki GitLab Action plugin](https://github.com/pepitosoft/wikka_action_gitlab)
- [WikkaWiki GitHub Action plugin](https://github.com/pepitosoft/wikka_action_github)

- Develop your own software.

_
====

## Deploy WikkaWiki
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/wikka_logo.jpg "WikkaWiki is a flexible, standards-compliant and lightweight wiki engine written in PHP.")](https://hub.docker.com/u/oems/)

DockerBuild repository:

[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/dockerhub.png "WikkaWiki Docker repo")](https://hub.docker.com/u/oems/)

- Deploy your apps.

_
====


## Continuous(Integration,Deployment,Delivery)
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/WikkaWiki_CI.png "Continuous(Integration,Deployment,Delivery)  ")](http://www.devops4all.com/2016/04/continuous-integration-continuous.html)

How Apply to the project.

_
====

## Complete WikkaWiki Demo
[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/wikka_logo.jpg "WikkaWiki is a flexible, standards-compliant and lightweight wiki engine written in PHP.")](http://wiki.pepitosoft.com/)

[http://wiki.pepitosoft.com/](http://wiki.pepitosoft.com/)

DockerBuild repository:

[![WikkaWiki](https://gitlab.com/iush/software-architecture-docs/raw/master/images/bluemix.png "WikkaWiki Blemix Deploy")](https://www.ibm.com/cloud-computing/bluemix/)

- Deploy your apps on Cloud.

----

## GitLab Ecosystem Overview
[![Coding the Next Build](https://gitlab.com/iush/software-architecture-docs/raw/master/images/gitlab_run.png "Software Architecture Patterns")](http://www.slidedeck.io/codethebuild/slides)

Only drivers and not database engine are installed on this Container.

----

## DockerHub Ecosystem Overview
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/docker-hub-diagram.png "Different types of blueprints")](https://blog.docker.com/2015/09/docker-hub-2-0/)

Update Software architecture descriptions, which are analogous to the different types of blueprints made in building architecture.

_
====

## DockerHub/Registries DEV/QA/CI
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/virtual-container-docker-9-638.jpg "Different types of blueprints")](https://www.slideshare.net/VenkataNagaRavi/virtual-container-docker)

Update Software architecture descriptions, which are analogous to the different types of blueprints made in building architecture.

_
====

## Principal Layered architecture and Docker-Composer
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/sapr_0101.png "Software Architecture Patterns")](https://www.safaribooksonline.com/library/view/software-architecture-patterns/9781491971437/)

The Composer, a set of containers on the Servicie build.

_
====

## Principal Layered architecture and DockerFile
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/sapr_0102.png "Software Architecture Patterns")](https://www.safaribooksonline.com/library/view/software-architecture-patterns/9781491971437/)

Only drivers and not database engine are installed on this Container.

_
====

## How is build this document:
[![Architecture viewpoints](https://gitlab.com/iush/software-architecture-docs/raw/master/images/gitlab_ci.png "GitLab CI")](https://gitlab.com/pepitosoft/pepitosoft.gitlab.io/blob/master/.gitlab-ci.yml)

This document use a CI env with Jekyll.

----

### Reference links

- [General document Software engineering](http://www.pepitosoft.com/jekyll/hwarch/posts/ingenieria-software-finalwork/)
- [Virtual Container - Docker](https://www.slideshare.net/VenkataNagaRavi/virtual-container-docker)
- [Original View Model of Software Architecture ](http://www.cs.ubc.ca/~gregor/teaching/papers/4+1view-architecture.pdf)
- [Software Architecture for Agile Developers Intro](https://www.slideshare.net/ChrisCarroll2/xpmanchester-2013-software-architecture-for-agile-developers-intro)
- [Wikipedia](https://en.wikipedia.org/wiki/Software_architecture)
- [An empirical analysis of the Docker container ecosystem on GitHub](https://peerj.com/preprints/2905/)
- [Docker: Build, Ship and Run Any App, Anywhere](https://delftswa.github.io/chapters/docker/)
- [The Architecture of Open Source Applications](http://aosabook.org/en/index.html)
- [Software Architecture
Patterns](http://www.oreilly.com/programming/free/files/software-architecture-patterns.pdf), O’Reilly Book.
- [Coding the Next Build](http://www.slidedeck.io/codethebuild/slides)
- [Gitlab meets Kubernetes](https://www.slideshare.net/inovex/gitlab-meets-kubernetes)
- [Continuous(Integration,Deployment,Delivery)](http://www.devops4all.com/2016/04/continuous-integration-continuous.html)

_
====

### Reference links

- [Continuous Integration, Delivery, and Deployment with GitLab](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/)
